# JJ-array simulations

Repository for bachelor project on Josephson junction array simulations.

## Description of files

* visualization.py (redundant, for now)
	* Provides visualization of (disordered) array of Josephson junctions.

* IV_curves.py
	* Calculates one or more IV curves for a given (disordered) array.

* functions.py
	* Contains all the functions used in the files above.
	
* analyse_data.py
    * File used for loading pickled IV curves and plotting them.

* clusterstart.bat
	* Used to start a cluster of engines used for computing IV curves in parallel.

* clusterstop.bat
	* Used to shut down the aforementioned cluster.
	
## Changelog

### 4-10-2019: version 3.6
* Added analyse_data.py for loading and plotting IV curves.

### 25-9-2019: version 3.5
* Previous version was broken, everything is fixed now.
* One can choose between parallelization of IV curve calculations, or parallelization of each individual IV curve calculation.
* Added generate_Ic() and get_constants().

### 18-9-2019: version 3.4
* Single IV-curve calculation is now also parallelized. Program is ready for use on Linux servers.

### 16-9-2019: version 3.3
* IVs and matplotlib figures are now saved as well, and the runtime is also recorded in the logbook.

### 12-9-2019: version 3.2
* Figures are now automatically saved, and parameters recorded in logbook.txt.

### 4-9-2019: version 3.0
* Added README.
* Restructured the whole project, improved usability. Removed redundant functions. Didn't extensively test.

## TODO