# -*- coding: utf-8 -*-
"""
======================================
 JOSEPHSON JUNCTION ARRAY SIMULATIONS
======================================
v3.6
T.S. Reintsema
Interfaces and Correlated Electrons group, University of Twente
4-10-2019

===================
 ANALYSING IV-DATA
===================
"""
#%% Imports
import numpy as np
import matplotlib.pyplot as plt
import pickle
from PyQt5 import QtWidgets


#%% Functions

def open_file():
    options = QtWidgets.QFileDialog.Options()
    options |= QtWidgets.QFileDialog.DontUseNativeDialog
    filename, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Open an IV curve file", "","Pickle Files (*.pickle)", options=options)
    if filename:
        return filename
    
    
def get_data(filename):
    with open(filename, 'rb') as loadfile:
        IVs = pickle.load(loadfile)
    return IVs
    
    
def plot_data(IVs):
    plt.figure()
    legend = []
    i = 0
    for I, V in IVs:
        plt.plot(I, V)
        legend.append(i)
        i += 1
    plt.legend(legend)
    plt.xlabel("I")
    plt.ylabel("V")


#%% Main
def main():
    filename = open_file()
    IVs = get_data(filename)
    plot_data(IVs, filename)
        
if __name__ == "__main__":
    main()
    
    
    
    
    
    
    
    
    
    
    
    
