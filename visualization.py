# -*- coding: utf-8 -*-
"""
======================================
 JOSEPHSON JUNCTION ARRAY SIMULATIONS
======================================
v3.0
T.S. Reintsema
Interfaces and Correlated Electrons Group, University of Twente
4-9-2019

======================================================================
 Visualization of a positionally disordered Josephson junction arrays
======================================================================
This file is for producing visualizations (animated or still frames) of
Josephson junction arrays that may be positionally disordered.

"""

import numpy as np
import matplotlib.pyplot as plt

from functions import (disordered_array,
                       calculate,
                       visualization_still,
                       visualization_animated,)


show_animation = True
show_stills = False
show_phase = False
show_vortices = False

#%%# ARRAY PARAMETERS #%%#
N = 16
T = 0.01
n0 = 2
Ibar_h = .8
Ibar_v = 0
steps = 1000
dt = 0.5

r = 0.2
Tc = 1
Ic0 = 1

Ic_h, Ic_v, dh, dv, xs, ys = disordered_array(N, r, T, Tc, Ic0)


#%%# CALCULATIONS #%%#
(Umean_h, Umean_v, I_h, I_v, phi_h, phi_v, n, t) = calculate(N, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)

#%% RESULTS #%%#

if show_animation:
    plt.close(12)
    anim = visualization_animated(N, n, I_h, I_v, xs, ys, figure=12)

if show_stills:
    visualization_still(N, n, I_h, I_v, xs, ys, step=0, figure=10)
    visualization_still(N, n, I_h, I_v, xs, ys, step=-2, figure=20)
    plt.show()    

if show_phase:
    plt.figure(1)
    plt.clf()
    plt.plot(t, phi_h)
    plt.xlabel('t')
    plt.ylabel('phi_h')
    plt.title('Phase difference (in horizontal direction) as a function of time')
    
if show_vortices:
    plt.figure(2)
    plt.clf()
    plt.plot(t, np.reshape(n, (-1,N**2)))
    plt.xlabel('t')
    plt.ylabel('vortices')
    plt.title('Vortices as a function of time')
    