# -*- coding: utf-8 -*-
"""
======================================
 JOSEPHSON JUNCTION ARRAY SIMULATIONS
======================================
v3.5
T.S. Reintsema
Interfaces and Correlated Electrons group, University of Twente
25-9-2019

Function library for generating, visualizing and calculating (disordered)
Josephson junction arrays.

"""
#%%######## IMPORTS ###########################################################

import ipyparallel as ipp
import pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import subprocess
from matplotlib import animation
from numpy.fft import (fft2, ifft2)
from tqdm import tqdm

#%%######## INITIALIZATION ####################################################

def generate_Ic(Nsize, Ic0, sigma):
    '''
    '''
    if np.size(sigma) == 1:
        Ic_h = np.zeros((Nsize, Nsize))
        Ic_v = Ic_h.copy()
        Ic_h = np.random.normal(Ic0, sigma, (Nsize, Nsize))
        Ic_v = np.random.normal(Ic0, sigma, (Nsize, Nsize))

    else:
        Ic_h = np.zeros((np.size(sigma), Nsize, Nsize))
        Ic_v = Ic_h.copy()
        for i, s in enumerate(sigma):    
            Ic_h[i] = np.random.normal(Ic0, s, (Nsize, Nsize))
            Ic_v[i] = np.random.normal(Ic0, s, (Nsize, Nsize))

    Ic_h[np.where(Ic_h < 0)] = 0
    Ic_v[np.where(Ic_v < 0)] = 0
    
    return Ic_h, Ic_v


def get_constants():
    kB = 1.38064852E-23 # m2 kg s-2 k-1
    e = 1.60217662E-19 # C
    hbar = 1.0545718E-34 # m2 kg s-1
    Tconvert = 2*kB*e*hbar
    return kB, e, hbar, Tconvert


#%%######## LOGGING ###########################################################

def update_logbook(string):
    with open('./figures/logbook.txt', 'a+') as logbook:
        logbook.write(string)


def parameter_string(date, Nsize, T, n0, num_Ibar, Ic0, sigma, steps, dt):
    string = ''
    string += date.strftime('%Y-%m-%d %H:%M:%S %f') + '\n'
    string += 'Nsize = {}\n'.format(Nsize)
    string += 'T = {}\n'.format(T)
    string += 'n0 = {}\n'.format(n0)
    string += 'num_Ibar = {}\n'.format(num_Ibar)
    string += 'Ic0 = {}\n'.format(Ic0)
    string += 'sigma = {}\n'.format(sigma)
    string += 'steps = {}\n'.format(steps)
    string += 'dt = {}\n'.format(dt)
    
    return string


#%%######## CALCULATING FEATURES ##############################################

### Main function ###
def calculate(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt):
    '''
    Calculates average voltage in horizontal and vertical direction, current in
    horizontal and vertical direction, phase difference in the horizontal and 
    vertical direction and vortices for a square array of <Nsize>*<Nsize> 
    islands, at temperature <T>, with <n0> initial vortices (which results in 
    f = <n0>/(<Nsize>**2)), with external currents <Ibar_h> and <Ibar_v>, for 
    <steps> steps and time step <dt>.
    '''
    
    Nh = Nsize
    Nv = Nsize
    f = n0 / (Nh * Nv)
    z = np.zeros(Nh * Nv)
    z[:n0] = np.ones(n0)
    np.random.shuffle(z)
    z = np.reshape(z, (Nh, Nv))
    
    I_h = np.zeros((steps+1, Nh, Nv))
    I_v = np.zeros((steps+1, Nh, Nv))
    Ih_actual = np.zeros((steps+1, Nh, Nv))
    Iv_actual = np.zeros((steps+1, Nh, Nv))

    phi = np.zeros((steps+1, Nh, Nv))
    phi_h = np.zeros(steps+1)
    phi_v = np.zeros(steps+1)
    n = np.zeros((steps+1, Nh, Nv))

    Kbar = initialize_Kbar(Nh, Nv)
    K = fft2(Kbar)
    K[0,0] = 1
    
    lam = 2 * np.pi * ifft2(fft2(z-f) / K)
    g_h = lam - v_min(lam)
    g_v = h_min(lam) - lam
    
    for t in range(steps):
        eta_h = np.random.normal(0, np.sqrt(2 * T / dt), size=(Nv, Nh))
        eta_v = np.random.normal(0, np.sqrt(2 * T / dt), size=(Nv, Nh))
        
        theta_h = h_plus(phi[t]) - phi[t] + g_h + phi_h[t]
        theta_v = v_plus(phi[t]) - phi[t] + g_v + phi_v[t]

        I_h[t] = Ic_h * np.sin(theta_h) + eta_h - Ibar_h
        I_v[t] = Ic_v * np.sin(theta_v) + eta_v - Ibar_v

        phi_tilde = I_h[t] - h_min(I_h[t]) + I_v[t] - v_min(I_v[t])
        
        dphi = dt * ifft2(fft2(phi_tilde) / K)
        dphibar_h = - dt * np.mean(I_h[t])
        dphibar_v = - dt * np.mean(I_v[t])
        
        dtheta_h = h_plus(dphi) - dphi + dphibar_h
        dtheta_v = v_plus(dphi) - dphi + dphibar_v
        
        BTI_h = Ibar_h
        BTI_v = Ibar_v
        
        Ih_actual[t] = I_h[t] + dtheta_h + BTI_h
        Iv_actual[t] = I_v[t] + dtheta_v + BTI_v
        
        phi[t+1] = phi[t] + dphi
        phi_h[t+1] = phi_h[t] + dphibar_h
        phi_v[t+1] = phi_v[t] + dphibar_v
        
        theta_h = np.round(theta_h / (2 * np.pi))
        theta_v = np.round(theta_v / (2 * np.pi))
        
        n[t] = z - (theta_h - v_plus(theta_h) - theta_v + h_plus(theta_v))
        
    Umean_h = 1 / (dt*(steps-np.int(steps/2))) * (phi_h[steps] - phi_h[np.int(steps/2)])
    Umean_v = 1 / (dt*(steps-np.int(steps/2))) * (phi_v[steps] - phi_v[np.int(steps/2)])
        
    time = steps * dt
    t = np.linspace(0, time+dt, steps+1)
    
    return Umean_h, Umean_v, Ih_actual, Iv_actual, phi_h, phi_v, n, t


### Subfunctions ###
def initialize_Kbar(Nh, Nv):
    '''
    Creates the Kbar-matrix that is used in the algorithm for simulating arrays
    with periodic boundary conditions.
    '''
    Kbar = np.zeros((Nh, Nv))
    Kbar[0,0] = 4
    Kbar[0,1] = -1
    Kbar[1,0] = -1
    Kbar[0,Nh-1] = -1
    Kbar[Nv-1,0] = -1
    return Kbar


def v_plus(x):
    return np.roll(x, -1, axis=0)


def v_min(x):
    return np.roll(x, 1, axis=0)


def h_plus(x):
    return np.roll(x, -1, axis=1)


def h_min(x):
    return np.roll(x, 1, axis=1)


#%%######## CALCULATING IV CURVES #############################################
    
### Main functions ###
def IV_curves_parallel(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt):
    '''
    Calculates IV curves for as many different variables are given.
    Inputs don't have to have the same dimension, single values will
    automatically be cast to arrays as long as the longest input dimension.
    '''
    Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt = equalize_input_dimensions(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)        
    
    c = ipp.Client()
    v = c[:]
    IVs = v.map(IV_curve, Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
    
    return IVs


def IV_curves_altparallel(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt):
    Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt = equalize_input_dimensions(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
    num = np.size(T)
    
    IVs = np.zeros((num, 2, len(Ibar_h[0])))
    for i in range(num):
        (I, V) = IV_curve_parallel(Nsize[i], T[i], n0[i], Ibar_h[i], Ibar_v[i], Ic_h[i], Ic_v[i], steps[i], dt[i])
        IVs[i,0,:] = I
        IVs[i,1,:] = V
        
    return IVs


def IV_plots_from_list(IVs, figure, date, show=True, flip=False):
    '''
    Creates a figure containing IV plots taken from (I, V) pairs contained in
    the list <IVs>.
    '''
    fig = plt.figure(figure)
    plt.clf()
    i = 0
    legendlist = []
    for I, V in IVs:
        if not flip:
            plt.plot(V, I)
        else:
            plt.plot(I, V)
        legendlist.append(i)
        i+=1
    plt.legend(legendlist)
    plt.title('IV curves from Josephson junction array')
    
    if not flip:
        plt.xlabel('V')
        plt.ylabel('I')
    else:
        plt.xlabel('I')
        plt.ylabel('V')

    IVs = [(I, V) for I, V in IVs]
    with open('./figures/IVs/' + date.strftime('%Y-%m-%d %H%M%S %f') + ' IVs.pickle', 'wb') as savefile:
        pickle.dump(IVs, savefile)
    with open('./figures/fig/' + date.strftime('%Y-%m-%d %H%M%S %f') + ' fig.pickle', 'wb') as savefile:
        pickle.dump(fig, savefile)
    fig.savefig('./figures/png/' + date.strftime('%Y-%m-%d %H%M%S %f') + '.png', dpi = 1000)
    fig.savefig('./figures/pdf/' + date.strftime('%Y-%m-%d %H%M%S %f') + '.pdf')
    if show:
        fig.show()


### Subfunctions ###
def IV_curve(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt, show_progress=False):
    '''
    Returns I and V vectors for given T and n0.
    One of Ibar_h and Ibar_v should be an array, and will be taken as I-vector.
    '''
    
    if type(Ibar_h) is np.ndarray:
        Umean_h = np.zeros(np.size(Ibar_h))
        I = Ibar_h
        i = 0
        if show_progress:
            for Ibar_h in tqdm(Ibar_h):
                Umean_h[i], Umean_v, I_h, I_v, phi_h, phi_v, n, t = calculate(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
                i += 1
        else:
            for Ibar_h in Ibar_h:
                Umean_h[i], Umean_v, I_h, I_v, phi_h, phi_v, n, t = calculate(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
                i += 1
        V = Umean_h
    elif type(Ibar_v) is np.ndarray:
        Umean_v = np.zeros(np.size(Ibar_v))
        I = Ibar_v   
        i = 0
        if show_progress:
            for Ibar_v in tqdm(Ibar_v):
                Umean_h, Umean_v[i], I_h, I_v, phi_h, phi_v, n, t = calculate(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
                i += 1
        else:
            for Ibar_v in Ibar_v:
                Umean_h, Umean_v[i], I_h, I_v, phi_h, phi_v, n, t = calculate(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
                i += 1
        V = Umean_v
        
    return I, V


def IV_curve_parallel(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt):
    '''
    '''
    N = len(Ibar_h)
    
    Nsize = [Nsize for i in range(N)]
    T = [T for i in range(N)]
    n0 = [n0 for i in range(N)]
    Ibar_v = [Ibar_v for i in range(N)]
    Ic_h = [Ic_h for i in range(N)]
    Ic_v = [Ic_v for i in range(N)]
    steps = [steps for i in range(N)]
    dt = [dt for i in range(N)]
    
    c = ipp.Client()
    dview = c[:]
    dview.execute('import numpy as np')
    dview.execute('from numpy.fft import (fft2, ifft2)')
    dview.execute('from functions import (initialize_Kbar, v_plus, v_min, h_plus, h_min)')
    result = dview.map_sync(calculate, Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
    
    Umean_h = [Umean_h for (Umean_h, Umean_v, I_h, I_v, phi_h, phi_v, n, t) in result]
    
    V = Umean_h
    I = np.array(Ibar_h)
    
    return I, V


def start_cluster(engines=8):
    subprocess.Popen(['cmd.exe','/c ipcluster start -n {}'.format(engines)])


def stop_cluster():
    subprocess.Popen(['cmd.exe','/c iplcuster stop'])


def equalize_input_dimensions(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt):
    '''
    Equalizes the length of all variables to be as great as the longest input
    dimension.
    Note: Ic_h and Ic_v should both be (arrays of) Nsize by Nsize arrays, even
    if Ic is constant for all junctions.
    '''
    maxlength = 1
    for variable in [Nsize, T, n0, Ic_h, Ic_v, steps, dt]:
        if variable is Ic_h or variable is Ic_v:
            if len(variable.shape) < 3:
                length = 1
            else:
                length = np.size(variable, axis=0)
        else:
            length = np.size(variable)
        if length > maxlength:
            maxlength = length
        
    if np.size(Nsize) < maxlength:
        Nsize = [Nsize for i in range(maxlength)]
    if np.size(T) < maxlength:
        T = [T for i in range(maxlength)]
    if np.size(n0) < maxlength:
        n0 = [n0 for i in range(maxlength)]
    if len(Ic_h.shape) < 3:
        Ic_h = [Ic_h for i in range(maxlength)]
    else:
        if np.size(Ic_h, axis=0) < maxlength:
            Ic_h = [Ic_h for i in range(maxlength)]
    if len(Ic_v.shape) < 3:
        Ic_v = [Ic_v for i in range(maxlength)]
    else:
        if np.size(Ic_v, axis=0) < maxlength:
            Ic_v = [Ic_v for i in range(maxlength)]
    if np.size(steps) < maxlength:
        steps = [steps for i in range(maxlength)]
    if np.size(dt) < maxlength:
        dt = [dt for i in range(maxlength)]
        
    Ibar_h = [Ibar_h for i in range(maxlength)]
    Ibar_v = [Ibar_v for i in range(maxlength)]
            
    return Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt

    
#%%############################################################################
















#%%######## DEPRECATED ########################################################

#@DEPRECATED
#%######## GENERATING POSITIONALLY DISORDERED ARRAY ##########################

### Main function ###
def disordered_array(Nsize, r, T, Tc, Ic0):
    spacing = 1 - 2*r
    
    dh, dv, xco, yco = randomized_distances(Nsize, r, spacing)
    dh /= np.max(dh)
    dv /= np.max(dv)
    
    dh *= 1
    dv *= 1
    
    Ic_h = generate_Ic(Ic0, T, Tc, dh)
    Ic_v = generate_Ic(Ic0, T, Tc, dv)
    
    return Ic_h, Ic_v, dh, dv, xco, yco


### Subfunctions ###
def randomized_distances(Nsize, r, spacing):
    xco, yco = randomized_coordinates(Nsize, r, spacing)
    
    dh = np.zeros((Nsize, Nsize))
    dv = np.zeros((Nsize, Nsize))
    
    for i in range(Nsize):
        for j in range(Nsize):
            
            if j == Nsize-1:
                x_hcurrent = xco[i,j] - Nsize * (2*r + spacing)
                y_hcurrent = yco[i,j]
                
                x_hnext = xco[i,0]
                y_hnext = yco[i,0]
                
            else:
                x_hcurrent = xco[i,j]
                y_hcurrent = yco[i,j]
                
                x_hnext = xco[i,j+1]
                y_hnext = yco[i,j+1]
                
            if i == Nsize-1:
                x_vcurrent = xco[i,j] 
                y_vcurrent = yco[i,j] - Nsize * (2*r + spacing)
                
                x_vnext = xco[0,j]
                y_vnext = yco[0,j]
                
            else:        
                x_vcurrent = xco[i,j]
                y_vcurrent = yco[i,j]
                
                x_vnext = xco[i+1,j]
                y_vnext = yco[i+1,j]
                
            dh_x = x_hnext - x_hcurrent
            dh_y = y_hnext - y_hcurrent
            
            dv_x = x_vnext - x_vcurrent
            dv_y = y_vnext - y_vcurrent
            
            dh[i,j] = np.sqrt(dh_x**2 + dh_y**2)
            dv[i,j] = np.sqrt(dv_x**2 + dv_y**2) 
    
    return dh, dv, xco, yco
    

def randomized_coordinates(Nsize, r, spacing):    
    xco, yco = spaced_coordinates(Nsize, r, spacing)
    
    xco += np.random.uniform(-r, r, (Nsize, Nsize))
    yco += np.random.uniform(-r, r, (Nsize, Nsize))
    
    return xco, yco


def spaced_coordinates(Nsize, r, spacing):
    xco, yco = coordinates(Nsize) 
    
    xco *= (2*r + spacing)
    yco *= -(2*r + spacing)
    
    return xco, yco
   
    
def coordinates(Nsize):
    xco = np.zeros((Nsize, Nsize))
    yco = np.zeros((Nsize, Nsize))
    
    for r in range(Nsize):
        for c in range(Nsize):
            xco[r, c] = c
            yco[r, c] = r
            
    return xco, yco

#WIP
def generate_Ic2(Ic0, T, Tc, d):
    #Ic = Ic0 * (1 - T/Tc)**2 * np.exp(-d*np.sqrt(T))
    #Ic = Ic0 * (1 - T/Tc)**2 * np.exp(-d)
    
    Ic = d
    
    return Ic



#@DEPRECATED
#%######## VISUALIZATION #####################################################
    
### Main functions ###
def visualization_animated(N, ns, I_hs, I_vs, xs, ys, figure):
    # SET UP EMPTY FIGURE
    fig = plt.figure(figure)
    plt.clf()
    plt.cla()
    ax = plt.axes()
    ax.set_xlim(left=-0.5, right=N-0.5)
    ax.set_ylim(top=0.5, bottom=-N+0.5)
    ax.set_aspect(1)
    
    # GET TOTAL STEPS
    steps = np.size(ns, axis=0)    
    
    # NEW DRAW_ARROW DEFINITION
    def draw_arrow(x1, y1, x2, y2, w, r, ax, direction):
        '''
        Draws an arrow from (x1,y1) to (x2,y2) on given axes.
        '''
        
        dx = x2-x1
        dy = y2-y1
        vector = np.array([dx, dy])
        
        dx0 = dx / np.linalg.norm(vector) * r
        dy0 = dy / np.linalg.norm(vector) * r
        
        x1 += dx0
        x2 -= dx0
        y1 += dy0
        y2 -= dy0
            
        dx = x2-x1
        dy = y2-y1       
        arrow = mpatches.Arrow(x1,y1,dx,dy, width = w, color='gold')
        return ax.add_patch(arrow)    
    
    # PLOT ISLANDS
    for x, y in zip(np.reshape(xs, N**2), np.reshape(ys, N**2)):
        circle = mpatches.Circle((x, y), radius=0.15)
        ax.add_patch(circle)
        
    
    def init():
        return []
    
    
    def animate(step):
        patches = []
        
        n = ns[step]
        I_h = I_hs[step]
        I_v = I_vs[step]
        
        maxcurrent = np.max([np.max(np.abs(I_h)), np.max(np.abs(I_v))])
        norm_Ih = I_h / maxcurrent
        norm_Iv = I_v / maxcurrent
        maxwidth = 0.5
        wh = norm_Ih * maxwidth
        wv = norm_Iv * maxwidth    
        
        # PLOT CURRENTS
        x, y = xs, ys
        s = 0.15
        for r in range(N):
            for c in range(N):
                # HORIZONTAL
                if c == (N-1):
                    # FIRST
                    x1 = x[r,c]
                    y1 = y[r,c]
                    x2 = x[r,0] + N
                    y2 = y[r,0]
                    
                    w = wh[r,c]
                    if I_h[r,c] > 0:
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'h'))
                    else:
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'h'))
                    
                    # SECOND
                    x1 -= N
                    x2 -= N
                    if I_h[r,c] > 0:
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'h'))
                    else:
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'h'))
                    
                else:
                    x1 = x[r,c]
                    y1 = y[r,c]
                    x2 = x[r,c+1]
                    y2 = y[r,c+1]
                    
                    w = wh[r,c]
                    if I_h[r,c] > 0:    
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'h'))
                    else:                                                     
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'h'))
                    
                # VERTICAL
                if r == (N-1):
                    # FIRST
                    x1 = x[r,c]
                    y1 = y[r,c]
                    x2 = x[0,c]
                    y2 = y[0,c] - N
                    
                    w = wv[r,c]
                    if I_v[r,c] > 0:
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'v'))
                    else:
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'v'))
                    
                    # SECOND
                    y1 += N
                    y2 += N
                    if I_h[r,c] > 0:
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'v'))
                    else:
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'v'))
                        
                else:
                    x1 = x[r,c]
                    y1 = y[r,c]
                    x2 = x[r+1,c]
                    y2 = y[r+1,c]
                    
                    w = wv[r,c]
                    if I_v[r,c] > 0:
                        patches.append(draw_arrow(x1, y1, x2, y2, w, s, ax, 'v'))
                    else:
                        patches.append(draw_arrow(x2, y2, x1, y1, w, s, ax, 'v'))
    
        # PLOT VORTICES
        for r in range(N):
            for c in range(N):
                if n[r,c]:
                    
                    if c == N-1:
                        x_hnext = x[r,0] + N
                        y_hnext = y[r,0]
                    else:
                        x_hnext = x[r,c+1]
                        y_hnext = y[r,c+1]
                    
                    if r == N-1:
                        x_vnext = x[0,c]
                        y_vnext = y[0,c] - N
                        
                    else:
                        x_vnext = x[r+1,c]
                        y_vnext = y[r+1,c]
                    
                    
                    x0 = 0.5 * (x_hnext + x_vnext)
                    y0 = 0.5 * (y_hnext + y_vnext)
                    
                    rd = 0.2
                    vortex = mpatches.Circle((x0,y0), radius=rd, fill=False, 
                                     color='orange', linewidth=3)
                    patches.append(ax.add_patch(vortex))
                    
                    if c == N-1:
                        extravortex = mpatches.Circle((x0-N,y0), radius=rd, fill=False, 
                                     color='orange', linewidth=3)
                        patches.append(ax.add_patch(extravortex))
                                        
                    if r == N-1:
                        extravortex = mpatches.Circle((x0,y0+N), radius=rd, fill=False, 
                                     color='orange', linewidth=3)
                        patches.append(ax.add_patch(extravortex))
                        
        patches.append(ax.text(0.05, 0.1, '{} / {}'.format(step, steps),
                       horizontalalignment='left',
                       verticalalignment='center',
                       fontsize=15, color='black',
                       transform=ax.transAxes))
        
        return patches
    
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=steps, interval=10, blit=True)
    
    plt.show()
    
    return anim


def visualization_still(N, n, I_h, I_v, xs, ys, step, figure):
    # SET UP EMPTY FIGURE
    plt.figure(figure)
    plt.clf()
    ax = plt.gca()
    ax.set_xlim(left=-0.5, right=N-0.5)
    ax.set_ylim(top=0.5, bottom=-N+0.5)
    ax.set_aspect(1)
    
    # SET UP CURRENTS, VORTICES TO BE USED
    steps = np.size(n, axis=0)
    n = n[step]
    I_h = I_h[step]
    I_v = I_v[step]
    maxcurrent = np.max([np.max(np.abs(I_h)), np.max(np.abs(I_v))])
    norm_Ih = I_h / maxcurrent
    norm_Iv = I_v / maxcurrent
    maxwidth = 0.5
    wh = norm_Ih * maxwidth
    wv = norm_Iv * maxwidth
    
    # NEW DRAW_ARROW DEFINITION
    def draw_arrow(x1, y1, x2, y2, w, r, ax, direction):
        '''
        Draws an arrow from (x1,y1) to (x2,y2) on given axes.
        '''
        
        dx = x2-x1
        dy = y2-y1
        vector = np.array([dx, dy])
        
        dx0 = dx / np.linalg.norm(vector) * r
        dy0 = dy / np.linalg.norm(vector) * r
        
        x1 += dx0
        x2 -= dx0
        y1 += dy0
        y2 -= dy0
            
        dx = x2-x1
        dy = y2-y1       
        arrow = mpatches.Arrow(x1,y1,dx,dy, width = w, color='gold')
        ax.add_patch(arrow)
    
    # PLOT ISLANDS
    for x, y in zip(np.reshape(xs, N**2), np.reshape(ys, N**2)):
        circle = mpatches.Circle((x, y), radius=0.15)
        ax.add_patch(circle)
        
    # PLOT CURRENTS
    x, y = xs, ys
    s = 0.15
    for r in range(N):
        for c in range(N):
            # HORIZONTAL
            if c == (N-1):
                # FIRST
                x1 = x[r,c]
                y1 = y[r,c]
                x2 = x[r,0] + N
                y2 = y[r,0]
                
                w = wh[r,c]
                if I_h[r,c] > 0:
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'h')
                else:
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'h')
                
                # SECOND
                x1 -= N
                x2 -= N
                if I_h[r,c] > 0:
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'h')
                else:
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'h')
                
            else:
                x1 = x[r,c]
                y1 = y[r,c]
                x2 = x[r,c+1]
                y2 = y[r,c+1]
                
                w = wh[r,c]
                if I_h[r,c] > 0:    
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'h')
                else:                                                     
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'h')
                
            # VERTICAL
            if r == (N-1):
                # FIRST
                x1 = x[r,c]
                y1 = y[r,c]
                x2 = x[0,c]
                y2 = y[0,c] - N
                
                w = wv[r,c]
                if I_v[r,c] > 0:
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'v')
                else:
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'v')
                
                # SECOND
                y1 += N
                y2 += N
                if I_h[r,c] > 0:
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'v')
                else:
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'v')
                    
            else:
                x1 = x[r,c]
                y1 = y[r,c]
                x2 = x[r+1,c]
                y2 = y[r+1,c]
                
                w = wv[r,c]
                if I_v[r,c] > 0:
                    draw_arrow(x1, y1, x2, y2, w, s, ax, 'v')
                else:
                    draw_arrow(x2, y2, x1, y1, w, s, ax, 'v')
        
    # PLOT VORTICES
    for r in range(N):
        for c in range(N):
            if n[r,c]:
                
                if c == N-1:
                    x_hnext = x[r,0] + N
                    y_hnext = y[r,0]
                else:
                    x_hnext = x[r,c+1]
                    y_hnext = y[r,c+1]
                
                if r == N-1:
                    x_vnext = x[0,c]
                    y_vnext = y[0,c] - N
                    
                else:
                    x_vnext = x[r+1,c]
                    y_vnext = y[r+1,c]
                
                
                x0 = 0.5 * (x_hnext + x_vnext)
                y0 = 0.5 * (y_hnext + y_vnext)
                
                rd = 0.2
                vortex = mpatches.Circle((x0,y0), radius=rd, fill=False, 
                                 color='orange', linewidth=3)
                ax.add_patch(vortex)
                
                if c == N-1:
                    extravortex = mpatches.Circle((x0-N,y0), radius=rd, fill=False, 
                                 color='orange', linewidth=3)
                    ax.add_patch(extravortex)
                                    
                if r == N-1:
                    extravortex = mpatches.Circle((x0,y0+N), radius=rd, fill=False, 
                                 color='orange', linewidth=3)
                    ax.add_patch(extravortex)
        
    ax.set_xlabel('frame {} of {}'.format(step, steps))  











