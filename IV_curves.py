# -*- coding: utf-8 -*-
"""
======================================
 JOSEPHSON JUNCTION ARRAY SIMULATIONS
======================================
v3.5
T.S. Reintsema
Interfaces and Correlated Electrons Group, University of Twente
25-9-2019
 
============================================================================
 Calculating IV-curves of Josephson junction arrays with varying parameters
============================================================================
This file is for calculating IV-curves of Josephson junction arrays with
varying <Nsize>, <T>, <n0>, <Ic_h>, <Ic_v>, <steps> and <dt>. For each of
these, one can either define a single value or an array of values, and the
program will automatically generate multiple IV-curves correspondingly.
For example, if <T> is an array of five values and all the other variables are
single values, five IV-curves will be calculated and shown, one for each value
of <T>, with the other variables being constant.

"""

import datetime
import numpy as np
from time import time
from functions import (IV_curves_parallel,
                       IV_curves_altparallel,
                       IV_plots_from_list,
                       parameter_string,
                       update_logbook,
                       get_constants,
                       generate_Ic)

kB, e, hbar, Tconvert = get_constants()
t1 = time()


#%%# ARRAY PARAMETERS #%%#

altparallel = True

Nsize = 16
T = 0
n0 = 0
num_Ibar = 200
Ibar_h = np.linspace(0, 2, num_Ibar)
Ibar_v = 0
steps = 100000
dt = 0.0005
sigma = np.array([0, 0.1, 0.2, 0.3, 0.4])
Ic0 = 1
Ic_h, Ic_v = generate_Ic(Nsize, Ic0, sigma)

# Log this experiment
date = datetime.datetime.now()
parameters = parameter_string(date, Nsize, T, n0, num_Ibar, Ic0, sigma, steps, dt)
update_logbook(parameters)


#%%# CALCULATIONS #%%#

if altparallel:
    fig = -1
    IVs = IV_curves_altparallel(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)
else:
    fig = 1
    IVs = IV_curves_parallel(Nsize, T, n0, Ibar_h, Ibar_v, Ic_h, Ic_v, steps, dt)


#%%# RESULTS #%%#
IV_plots_from_list(IVs, fig, date, False, True)


#%% Investigate speed because we like that
t2 = time()
runtime = t2-t1

print('Calculations took {} seconds.'.format(runtime))

timestring = 'runtime = ' + str(runtime) + '\n'
timestring += '\n'
update_logbook(timestring)

